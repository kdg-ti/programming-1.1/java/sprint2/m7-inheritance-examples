package be.kdg.oo.abstracts;

import be.kdg.oo.abstracts.model.Book;
import be.kdg.oo.abstracts.model.Product;

public class AbstractDemo {
    public static void main(String[] args) {
        Book book = new Book("NWB1", 13);
//        We cannot make a new Product because Product is abstract
//        Product book = new Product("PRD1", 13); // does not work
        book.promote();
        book.sell();
    }
}
