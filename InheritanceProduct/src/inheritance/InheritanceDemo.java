package inheritance;

import inheritance.model.Book;
import inheritance.model.Software;

public class InheritanceDemo {
    public static void main(String[] args) {
//        Make Book en Software objects, with  common and specific attributes
        Book book = new Book("book", "some book", 15.95, "John Doe","My life");
        Software sw = new Software("sw", "great quality software",99.95, "1.0.0");

//       Even though Book and Software do not have a "description" attribute themselves, you CAN call getDescription() on them
//       because it is inherited from superklasse Product
        System.out.println(book.getDescription());
        System.out.println(sw.getDescription());
    }
}
