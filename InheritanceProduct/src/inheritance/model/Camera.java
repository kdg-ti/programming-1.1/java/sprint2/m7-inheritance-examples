package inheritance.model;

public class Camera extends Product{
	private int pixels;

	public Camera(String code, String description, double price, int pixels) {
		super(code, description, price);
		this.pixels = pixels;
	}


	public int getPixels() {
		return pixels;
	}

	public void setPixels(int pixels) {
		this.pixels = pixels;
	}

	/*
	 * Method(s)
	 */
	@Override
	public String toString() {
		return super.toString() + String.format(" pixels:%d "
			, getPixels());
	}  // toString()

}
