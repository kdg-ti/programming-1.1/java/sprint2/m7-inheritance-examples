package finals;

public class Book {
    protected String title;
    protected String author;

    public Book(String title, String author) {
        this.title = title;
        this.author = author;
    }

    public final void read() {
        System.out.printf("Reading %s written by %s...", title, author);
    }
}
