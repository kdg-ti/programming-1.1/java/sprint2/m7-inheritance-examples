package finals;

public final class PrintedBook extends Book {
    private int pages;
    public PrintedBook(String title, String author, int pages) {
        super(title, author);
        this.pages = pages;
    }

    public int getPages() {
        return pages;
    }

//    You can not override read() because it is final in the superclass!

//    De compiler does not allow this:
//    public void read(){
//        System.out.println("this can not be customized");
//    }
}
