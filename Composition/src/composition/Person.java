package composition;

/**
 * Author: Jan de Rijke
 */
public class Person {
	char gender;
	Phone mobile;

	public Person(char gender, Phone mobile) {
		this.gender = gender;
		this.mobile = mobile;
	}
}
