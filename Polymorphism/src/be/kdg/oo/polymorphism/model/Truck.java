package be.kdg.oo.polymorphism.model;

public class Truck extends Vehicle {
    @Override
    public void drive() {
        System.out.println("18 wheels a rollin'...");
    }

    public void load() {
        System.out.println("Loading truck...");
    }

    public void unload() {
        System.out.println("Unloading...");
    }
}
