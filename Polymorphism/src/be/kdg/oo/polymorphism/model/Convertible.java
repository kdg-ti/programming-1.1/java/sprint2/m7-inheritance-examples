package be.kdg.oo.polymorphism.model;

public class Convertible extends Car {
    private boolean isOpen;

    public void openOrCloseRooftop() {
        if (isOpen){
            System.out.println("Closing rooftop");
        } else {
            System.out.println("Opening rooftop");
        }
    }
}
