package be.kdg.oo.polymorphism;

import be.kdg.oo.polymorphism.model.*;

public class PolymorphismDemo {
    public static void main(String[] args) {
        Vehicle convertible = new Convertible();
        Vehicle truck = new Truck();

        // Only Vehicle methods are available
        // Which implementation is used?
        convertible.drive();
        truck.drive();
//        convertible.openOrCloseRooftop(); // compile error!

        // After casting you can use specific methods of subclasses
        Convertible realConvertible = (Convertible)convertible;
        realConvertible.openOrCloseRooftop();

        Vehicle[] vehicles = {
                new Truck(),
                new Bus(35),
                new Car(),
                new Suv(),
                new Convertible()
        };

        for (Vehicle vehicle : vehicles) {
            vehicle.drive();
            if (vehicle instanceof Suv suv){
                suv.toggleAllWheelDrive();
                suv.drive();
            }
        }
    }
}
